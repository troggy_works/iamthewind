var speed : float = 2.0;

var xAmp : float = 0.5;
var yAmp : float = 1.0;
var xOffset : float = 1.0;
var yTurn : float = 1.0;
var tExp : float = 1.0;
var spiralOffset : float;

var maxTime : float = 10.0;

private var t : float = 0.0;
private var offset : Vector2 = Vector2.zero;

function Start(){
	t = maxTime;
}

function Update(){
	t -= Time.deltaTime * speed;
	if(t <= 0.0) Destroy(transform.root.gameObject, 1.0);
		
	var x : float = xOffset + (Mathf.Pow(t, tExp)) * xAmp * Mathf.Cos(t - spiralOffset);
	var y : float = (Mathf.Pow(t, tExp) * yAmp * Mathf.Sin(t - spiralOffset + yTurn));
	
	if(offset == Vector2.zero){
		offset = Vector2(x, y);
	}
	
	transform.localPosition.x = x - offset.x;
	transform.localPosition.y = y - offset.y;
}

/*private var a : float = 16.29;
private var b : float = 8.0;
private var c : float = 5.0;
private var d : float = 0.3;

var xMult : float = 2.0;
var yMult : float = 1.0;
var zMult : float = 3.0;

private var x : float = 0.1;
private var y : float = 0.1;
private var z : float = 0.1;

var rot : Vector3 = Vector3(30, 110, 0);
var scale : float;

@HideInInspector var oldX : float = 0.1;
@HideInInspector var oldY : float = 0.1;
@HideInInspector var oldZ : float = 0.1;


function Update () {
	Debug.DrawRay(transform.position, transform.parent.up*10, Color.green);
	x = oldX + a * d * (oldY - oldX) * Time.deltaTime * speed * xMult;
	y = oldY + d * (b * oldX - oldY - oldZ * oldX) * Time.deltaTime * speed * yMult;
	z = oldZ + d * (x * oldY  - c * oldZ) * Time.deltaTime * speed * zMult;
	
	oldX = x;
	oldY = y;
	oldZ = z;
	
	transform.localPosition.x = x;
	transform.localPosition.y = y;
	transform.localPosition.z = z;
	transform.localPosition = Quaternion.Euler(rot) * transform.position;
	transform.localPosition *= scale;
}*/