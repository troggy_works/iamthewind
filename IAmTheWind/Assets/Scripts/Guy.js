var animator : Animation;

var walkSpeed : float = 1.0;
var jumpForce : Vector3 = Vector3.up;
var minimumAmbientLight : float;
var graphics : Transform; //player graphics

var turnCastBuffer : float = 0.1;
var jumpCastOffset : float = 2.0;
var jumpCollideCastWidth : float = 0.1;
var groundCastDistance : float = 0.1;
var maxStepAngle : float = 45.0;
var willFallCastDistance : float = 10.0;

var layerMask : LayerMask;

var isAsleep : boolean = false;

var sleepEmitter : ParticleSystem;

private var walkDir : float = 1.0;
private var isGrounded : boolean = false;
private var cc : CapsuleCollider;

//casting variables
private var headVector : Vector3;
private var turnCastLength : float;
private var jumpCastVector : Vector3;

private var isBlocked : boolean = false;
private var canJump : boolean = true;
private var willFall : boolean = false;
private var halt : boolean = false;
private var jumpingIntoObject : boolean = false;
private var jumpTimedOut : boolean = false;
private var canSee : boolean = false;
private var shouldMoveTimeout : float = 1.0;
private var shouldMove : boolean = true;
private var sleepMoveTime : float = 0.0;

private var isFalling : boolean = false;
private var lastY : float;

function Start(){
	cc = GetComponent.<Collider>();
	headVector = Vector3.up * (cc.height / 2) * transform.localScale.y;
	turnCastLength = transform.localScale.x * cc.radius + turnCastBuffer;
	jumpCastVector = headVector + Vector3.up * jumpCastOffset;
	transform.eulerAngles.y = 90;
	
	if(animator){
		animator["Jump"].wrapMode = WrapMode.Loop;
		animator["Fall"].wrapMode = WrapMode.Loop;
		animator["WalkAwake"].wrapMode = WrapMode.Loop;
		animator["Walk"].wrapMode = WrapMode.Loop;
	}
}

function FixedUpdate () {
	var castHit : RaycastHit;
	var blockCastHit : RaycastHit;
	isGrounded = Physics.Raycast(transform.position, Vector3.down, castHit, groundCastDistance, layerMask); //if we're in contact with the ground, move along the normal
	isBlocked = Physics.Raycast(transform.position, Vector3.right * walkDir, blockCastHit, turnCastLength, layerMask);
	if(isBlocked) if(Vector3.Angle(Vector3.right * walkDir, blockCastHit.normal) < maxStepAngle) isBlocked = false;
	canJump = !Physics.Raycast(transform.position + jumpCastVector, Vector3.right * walkDir, turnCastLength + 0.1, layerMask);
	willFall = isBlocked ? false : !Physics.Raycast(transform.position + Vector3.right * walkDir * turnCastLength, Vector3.down, willFallCastDistance, layerMask);
	jumpingIntoObject = Physics.Raycast(transform.position + Vector3.right * walkDir * cc.radius, Vector3.right * walkDir, jumpCollideCastWidth, layerMask);
	
	canSee = (RenderSettings.ambientLight.r  + RenderSettings.ambientLight.b + RenderSettings.ambientLight.g) > minimumAmbientLight;
	sleepEmitter.enableEmission = isAsleep;
	
	if(isAsleep) {
		canSee = false; //OUR DUDE IS SLEEP WALKING
		if(sleepMoveTime > 0.0) shouldMove = true;
		//else shouldMove = false;
		sleepMoveTime -= Time.deltaTime;
	} else {
		shouldMove = true;
		sleepMoveTime = shouldMoveTimeout;
	}
	
	halt = false;
	
	if(!canSee) {
		willFall = false; //can't see edges
	}
	
	if(isBlocked){ //if we're blocked
		if(canJump && isGrounded &! jumpTimedOut){ //then see if we can jump
			Jump();
		} else { //otherwise turn around
			if(!jumpingIntoObject &! canJump){ //if we're grounded, turn around
				walkDir = -walkDir;
			} else if(jumpingIntoObject) {
				halt = true;
			}
		}
	} else { //if we're not blocked, we need to make sure we don't fall
		if(willFall) walkDir = -walkDir;
	}
	if(!halt && shouldMove) GetComponent.<Rigidbody>().MovePosition(transform.position + Vector3.right * walkDir * walkSpeed);
	isFalling = (lastY > transform.position.y);
	
	if(graphics) graphics.localRotation = Quaternion.Euler(0, (walkDir + 1) / 2 * -180, 0);
	
	if(animator) Animate();
	
	lastY = transform.position.y;
	
	/*if(isGrounded) {
		rigidbody.MovePosition(rigidbody.position + Vector3.Cross(castHit.normal, Vector3.forward) * walkDir * walkSpeed);
	} else rigidbody.MovePosition(rigidbody.position + Vector3.right * walkDir * walkSpeed);

	//cast to see if there's something in front of us
	if(Physics.Raycast(transform.position + Vector3.up * castHeight, Vector3.right * walkDir, castHit, horizontalCastDistance, layerMask)){
		if(Physics.Raycast(transform.position + Vector3.up * jumpCastHeight, Vector3.right * walkDir, horizontalCastDistance, layerMask)){ //if so, see if we can jump over it
			walkDir = -walkDir; //or just turn around
		} else { //if we can jump
			if(isGrounded) rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse); //jump!
		}
	} else {
		if(!Physics.Raycast(transform.position + Vector3.right * walkDir * ledgeWalkOffCastOffset, Vector3.down, ledgeCastDistance, layerMask)){
			walkDir = -walkDir; //turn around if we're about to walk off a ledge
		}
	}*/
}

function Animate(){
	if(isGrounded){
		if(shouldMove) { 
			if(!isAsleep){
				if(!animator.IsPlaying("WalkAwake")) animator.CrossFade("WalkAwake", 0.1);
			} else {
				if(!animator.IsPlaying("Walk")) animator.CrossFade("Walk", 0.1);
			}
		} else {
			if(!isAsleep){
				if(!animator.IsPlaying("IdleAwake")) animator.CrossFade("IdleAwake", 0.1);
			} else {
				if(!animator.IsPlaying("Idle")) animator.CrossFade("Idle", 0.1);
			}
		}
	} else {
		if(isFalling) {
			if(!animator.IsPlaying("Fall")){
				animator.CrossFade("Fall", 0.1);
			}
		} else {
			if(!animator.IsPlaying("Jump")){
				animator.CrossFade("Jump", 0.1);
			}
		}
	}
}

function Jump(){
	if(!jumpTimedOut) GetComponent.<Rigidbody>().AddForce(Vector3.Scale(jumpForce, Vector3(walkDir, 1, 1)), ForceMode.VelocityChange);
	jumpTimedOut = true;
	yield WaitForSeconds(0.1);
	jumpTimedOut = false;
}

function OnParticleCollision(col : GameObject){
	if(col.CompareTag("Rain")) isAsleep = false;
	if(col.CompareTag("Chimes")) isAsleep = true;
}

function Wind(force : Vector3){
	if(!isAsleep) return;
	if(sleepMoveTime > 0.0) return;
	walkDir = Mathf.Sign(force.x);
	sleepMoveTime = shouldMoveTimeout;
}

function OnDrawGizmos(){
	var col : CapsuleCollider = GetComponent(CapsuleCollider);
	//grounded ray
	Gizmos.color = Color.blue;
	Gizmos.DrawRay(transform.position, Vector3.down * groundCastDistance);
	//blocked ray
	Gizmos.color = Color.red;
	Gizmos.DrawRay(transform.position, Vector3.right * walkDir * turnCastLength); //blocked ray
	//blocked and need to jump? ray
	Gizmos.color = Color.green;
	Gizmos.DrawRay(transform.position + jumpCastVector, Vector3.right * walkDir * turnCastLength); //jump ray
	//will-fall-and-need-to-turn ray
	Gizmos.color = Color.magenta;
	Gizmos.DrawRay(transform.position + Vector3.right * walkDir * turnCastLength, Vector3.down * willFallCastDistance);	

	//
	Gizmos.color = Color.yellow;
	if(col) 
		Gizmos.DrawRay(transform.position + Vector3.right * walkDir * col.radius, Vector3.right * walkDir * jumpCollideCastWidth);
}