var triggerCollider : Collider;

var autoAssignTriggerTag : String = ""; //used to auto-assign if it's not manually assigned
var targetObject : GameObject;

var autoAssignTargetTag : String = "Controller";
var targetMessage : String;

var destroyTriggerObject : boolean = false;

var createObject : GameObject;
var createObjectPoint : Transform;

function Start(){
	if(!triggerCollider) {
		if(autoAssignTriggerTag) triggerCollider = GameObject.FindWithTag(autoAssignTriggerTag).GetComponent.<Collider>();
		else Debug.Log("No collider for " + gameObject.name);
	}
	if(!targetObject){
		if(autoAssignTargetTag) targetObject = GameObject.FindWithTag(autoAssignTargetTag);
		else Debug.Log("No target for " + gameObject.name);
	}
}

function OnTriggerEnter(col : Collider){
	var shouldDo : boolean = true;
	if(triggerCollider){ 
		if(col == triggerCollider) shouldDo = true; 
		else shouldDo = false;
	}
	
	if(shouldDo) {
		if(targetObject) targetObject.SendMessage(targetMessage);
		if(createObject) Instantiate(createObject, createObjectPoint.position, Quaternion.identity);
		if(destroyTriggerObject) {
			col.GetComponent.<Rigidbody>().MovePosition(Vector3.one * 10); //make sure ontriggerexit gets called where appropriate
			yield;
			Destroy(col.gameObject);
		}
	}
}