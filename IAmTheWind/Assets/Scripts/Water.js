var waterRange : Vector2 = Vector2.one;
var fillSpeed : float = 1.0;

private var curWaterHeight : float;
private var waterEmitter : ParticleEmitter;

private var lowVector : Vector3;
private var highVector : Vector3;

function Start () {
	lowVector = highVector = transform.position;
	lowVector.y -= waterRange.x;
	highVector.y += waterRange.y;
}

function Update(){
	transform.position = Vector3.Lerp(lowVector, highVector, curWaterHeight);
}

function OnParticleCollision(other : GameObject){
	if(other.CompareTag("Rain")) curWaterHeight = Mathf.Clamp(curWaterHeight + fillSpeed, 0.0, 1.0);
}