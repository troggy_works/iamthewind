var onObjects : GameObject[];
var onTexture : Texture2D;

private var offTexture : Texture2D;
private var powerTime : float;
private var isPowered : boolean = false;

function Start(){
	if(onTexture) offTexture = GetComponent.<Renderer>().material.mainTexture;
}

function Update () {
	powerTime -= Time.deltaTime;
	if(powerTime <= 0.0) if(isPowered) PowerOff();
}

function Power(){
	powerTime = 1.0;
	if(!isPowered) PowerOn();
}

function PowerOn(){
	isPowered = true;
	for(var curObject in onObjects){
		curObject.SetActiveRecursively(true);
	}
	if(onTexture) GetComponent.<Renderer>().material.mainTexture = onTexture;
}

function PowerOff(){
	isPowered = false;
	for(var curObject in onObjects){
		curObject.SetActiveRecursively(false);
	}
	if(onTexture) GetComponent.<Renderer>().material.mainTexture = offTexture;
}