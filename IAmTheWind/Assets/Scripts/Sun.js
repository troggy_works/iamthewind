var normalAmbientColor : Color;
var coveredAmbientColor : Color;
var normalFogColor : Color;
var normalFogIntensity : float = 10.0;
var coveredFogColor : Color;
var coveredFogIntensity : float = 0.0;

var normalLightIntensity : float = 1.0;
var coveredLightIntensity : float = 0.1;

var faces : Texture2D[];
var changeFaceTime : float = 10.0;
var faceRenderer : Renderer;

var sunlight : Light;
var layerMask : LayerMask;

private var castCam : Transform;
private var faceTime : float = 0.0;

function Start(){
	castCam = Camera.main.transform;
}

function Update(){
	if(!Physics.Raycast(transform.position, Vector3.forward, Mathf.Infinity, layerMask)){ //if an object is not in front of us
		RenderSettings.ambientLight = normalAmbientColor;
		RenderSettings.fogColor = normalFogColor;
		RenderSettings.fogStartDistance = normalFogIntensity;
		sunlight.intensity = normalLightIntensity;
	} else {
		RenderSettings.fogColor = coveredFogColor;
		RenderSettings.fogStartDistance = coveredFogIntensity;
		RenderSettings.ambientLight = coveredAmbientColor;
		sunlight.intensity = coveredLightIntensity;
	}
	
	faceTime -= Time.deltaTime;
	if(faceTime <= 0.0){
		if(faceRenderer.material.mainTexture == faces[0]){
			faceTime = 2.0;
			faceRenderer.material.mainTexture = faces[Random.Range(1, faces.Length)];
		} else {
			faceTime = changeFaceTime;
			faceRenderer.material.mainTexture = faces[0];
		}
	}
}