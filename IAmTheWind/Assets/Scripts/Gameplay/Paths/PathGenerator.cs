﻿//PathGenerator
//Takes a set of points and transforms them into a path suitable for wind to follow 

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IATW.Gameplay {

public delegate void OnPathUpdated(List<Vector3> path);

public interface IPathGenerator {
	event OnPathUpdated onPathUpdated;
	List<Vector3> path { get; set; }
	void AddSample(Vector3 sample, float timestamp);
	void Clear();
}

public class PathGenerator : IPathGenerator {
	public List<Vector3> path { get; set; }
	public event OnPathUpdated onPathUpdated;
	private Settings m_settings;

	public PathGenerator(Settings settings) {
		m_settings = settings;
		path = new List<Vector3>();
	}

	public void AddSample(Vector3 sample, float timestamp) {
		if(path.Count <= 1) {
			path.Add(sample);
			if(onPathUpdated != null) 
				onPathUpdated(path);
			return;
		}

		//if we haven't exceeded min distance from current sample to last sample just swap out last sample
		if((sample - path[path.Count - 2]).sqrMagnitude < m_settings.minDistanceBetweenPointsSqr) {
			path[path.Count - 1] = sample;
		} else {
			path.Add(sample);
		}
		if(onPathUpdated != null) 
			onPathUpdated(path);
	}

	public void Clear() {
		path.Clear();
		if(onPathUpdated != null) 
			onPathUpdated(path);
	}

	[Serializable]
	public class Settings {
		public float minDistanceBetweenPoints = 1.0f;
		public float minDistanceBetweenPointsSqr { get { return minDistanceBetweenPoints * minDistanceBetweenPoints; } }
	}
}

}
