﻿//PathGenerator
//Takes a set of points and transforms them into a path suitable for wind to follow 

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IATW.Gameplay {

public class LinePathGenerator : IPathGenerator {
	public List<Vector3> path { get; set; }
	public event OnPathUpdated onPathUpdated;
	private Settings m_settings;

	public LinePathGenerator(Settings settings) {
		m_settings = settings;
		path = new List<Vector3>();
	}

	public void AddSample(Vector3 sample, float timestamp) {
		if(path.Count <= 1) path.Add(sample);
		if(path.Count == 2) {
			RaycastHit hit;
			path[1] = Physics.Linecast(path[0], sample, out hit, m_settings.layerMask.value) ? hit.point : sample;
		}
		if(onPathUpdated != null) 
			onPathUpdated(path);
	}

	public void Clear() {
		path.Clear();
		if(onPathUpdated != null) 
			onPathUpdated(path);
	}

	[Serializable]
	public class Settings {	
		public LayerMask layerMask;
	}
}

}
