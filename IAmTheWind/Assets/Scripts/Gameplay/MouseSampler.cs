//MouseSampler
//Samples mouse each frame and generates sample events
//

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace IATW.Gameplay {

public delegate void OnSample(Vector3 sample, float timestamp);

public interface ISampler {
    event OnSample onSample;
    event Action onDown;
    event Action onUp;
    bool isDown { get; }
}

public class MouseSampler : ISampler, ITickable {
    public event OnSample onSample;
    public event Action onDown;
    public event Action onUp;

    public bool isDown { get { return Input.GetMouseButton(0); } }

    public void Tick()
    {
        if(Input.GetMouseButtonDown(0)) if(onDown != null) onDown();
        if(Input.GetMouseButtonUp(0)) if(onUp != null) onUp();

        if(onSample != null) {
            onSample(Input.mousePosition, Time.time);
        }
    }
}

}