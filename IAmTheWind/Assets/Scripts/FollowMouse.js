var moveSpeed : float = 1.0;

private var xVel : float = 0.0;
private var yVel : float = 0.0;

function Start(){
	Screen.lockCursor = true;
}

function Update(){
	xVel = Mathf.Lerp(xVel, 0.0, Time.deltaTime * moveSpeed * 0.5);
	xVel -= moveSpeed * Input.GetAxis("Mouse X");
	yVel = Mathf.Lerp(yVel, 0.0, Time.deltaTime * moveSpeed * 0.5);
	yVel += moveSpeed * Input.GetAxis("Mouse Y");
	transform.Translate(Vector3(xVel, yVel, 0.0) * Time.deltaTime);
}