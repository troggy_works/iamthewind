
@CustomEditor(Level)

class LevelEditor extends Editor{

function OnSceneGUI(){
	//Event.current.Use();
	if(!(target as Level).nodeGroups) return;
	
	for(var curGroup : NodeGroup in (target as Level).nodeGroups){
		if(!curGroup.displayEditor) continue;
		if(!curGroup.nodes) curGroup.ClearNodes();
		if(curGroup.nodes.length < 1) curGroup.ClearNodes();
		var handleSize : float = 5.0;// HandleUtility.GetHandleSize(curGroup.nodes[0].pos);

		Handles.color = Color.blue;
		curGroup.createNodePos = Handles.FreeMoveHandle(curGroup.createNodePos, Quaternion.identity, 0.3 * handleSize, Vector3.zero, Handles.SphereCap);
		Handles.color = Color.green;
		if(Handles.Button(curGroup.createNodePos + -Vector3.right * handleSize * 1, Quaternion.identity, 0.2 * handleSize, 1, Handles.CubeCap)) curGroup.AddNode();
		if(Handles.Button(curGroup.nodes[0].pos + Vector3.right * handleSize * 1, Quaternion.identity, 0.2 * handleSize, 1.0, Handles.CubeCap)) curGroup.UnshiftNode();
		
		var nodeIndex : int = 0;
		// NODE CYCLE
		for(var curNode : Node in curGroup.nodes){
			//prep
			if(curNode == curGroup.nodes[curGroup.nodes.length - 1]) break;
			var nextNode = curGroup.nodes[nodeIndex + 1];
			
			//node move handle
			Handles.color = Color.magenta;
			curNode.pos = Handles.FreeMoveHandle(curNode.pos, Quaternion.identity, 0.2 * handleSize, Vector3.one * 4, Handles.SphereCap);
			
			//node insert
			Handles.color = Color.green;
			if(Handles.Button(curNode.pos + Vector3.up + Vector3.forward, Quaternion.Euler(90, 0, 0), 0.2 * handleSize, 0.5, Handles.CylinderCap)) {
				curGroup.InsertNodeAtIndex(nodeIndex);
				break; //don't continue if the nodes have changed
			}
			
			//node kill
			if(((curNode.pos - nextNode.pos).magnitude) < 0.1) {
				curGroup.KillNodeAtIndex(nodeIndex);
				break;
			}
			
			//path break
			Handles.color = curNode.breakPath ? Color.green : Color.red;
			var rot : Quaternion = curNode.breakPath ? Quaternion.Euler(270, 0, 0) : Quaternion.Euler(90, 0, 0); 
			var lookRot : Quaternion = (nextNode.pos == curNode.pos) ? Quaternion.identity : Quaternion.LookRotation(nextNode.pos - curNode.pos);
			if(Handles.Button(Vector3.Lerp(curNode.pos, nextNode.pos, 0.5) + lookRot * Vector3.up * 2, lookRot * rot, 0.2 * handleSize, 0.5, Handles.ConeCap)){
				curNode.breakPath = !curNode.breakPath;
				curGroup.UpdateMesh();
			}
			
			//midpoint handle
			Handles.color = Color.white;
			var midpoint : Vector3 = Vector3.Lerp(curNode.pos, nextNode.pos, 0.5);
			var curNodeMidOffset : Vector3 = midpoint - curNode.pos;
			var nextNodeMidOffset : Vector3 = midpoint - nextNode.pos;
			midpoint = Handles.FreeMoveHandle(midpoint, Quaternion.identity, 0.1 * handleSize, Vector3.one, Handles.CylinderCap);
			curNode.pos = midpoint - curNodeMidOffset;
			nextNode.pos = midpoint - nextNodeMidOffset;
			
			//node break connector
			Handles.color = Color.blue;
			if(curNode.breakPath)	Handles.DrawLine(curNode.pos, nextNode.pos);
			
			//baseline adjustment
			if(!curNode.breakPath && curGroup.isMesh){
				var basePos : Vector3 = curNode.pos;
				basePos.y -= curNode.baselineDistance;
				var newPos : Vector3 = Handles.Slider(basePos, -Vector3.up);
				curNode.baselineDistance = curNode.pos.y - newPos.y;
			}
			
			nodeIndex++;
		}
				
		if(GUI.changed) {
			curGroup.UpdateMesh();
			curGroup.UpdateLastNodePos();
			curGroup.CreateColliders();
		}
	}
}

function OnInspectorGUI(){
//	DrawDefaultInspector();
	if(!(target as Level).nodeGroups) (target as Level).AddNodeGroup();
	if(GUILayout.Button("Add Node Group")) (target as Level).AddNodeGroup();
	
	for(var curGroup : NodeGroup in (target as Level).nodeGroups){
		if(curGroup.nodes.length < 1) curGroup.ClearNodes();
		if(Event.current.type == EventType.MouseDown) Undo.RegisterSceneUndo("Node Position");
		var content : GUIContent = new GUIContent(curGroup.displayName, curGroup.displayImage);
		curGroup.displayEditor = EditorGUILayout.Foldout(curGroup.displayEditor, content);
		
		if(curGroup.displayEditor){
			curGroup.displayName = EditorGUILayout.TextField("Name:", curGroup.displayName);
			curGroup.isMesh = EditorGUILayout.Toggle("Is Mesh:", curGroup.isMesh);
			curGroup.lockColliderZ = EditorGUILayout.FloatField("Collider Z Lock:", curGroup.lockColliderZ);
			curGroup.layer = EditorGUILayout.LayerField("Layer:", curGroup.layer);
			if(curGroup.isMesh){
				curGroup.material = (EditorGUILayout.ObjectField("Material:", curGroup.material, Material, false) as Material);
				GUILayout.BeginHorizontal();
				curGroup.tileSize = EditorGUILayout.Vector2Field("Tile Size:", curGroup.tileSize);
				curGroup.tileSize.x = Mathf.Max(1.0, curGroup.tileSize.x);
				curGroup.baselineTileSize = EditorGUILayout.Vector2Field("Tile Size:", curGroup.baselineTileSize);
				curGroup.baselineTileSize.x = Mathf.Max(1.0, curGroup.baselineTileSize.x);
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("UV X Range:", curGroup.uvRect.x.ToString() + ", " + curGroup.uvRect.z.ToString());
				EditorGUILayout.MinMaxSlider(curGroup.uvRect.x, curGroup.uvRect.z, 0.0, 1.0);
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("UV Y Range:", curGroup.uvRect.y.ToString() + ", " + curGroup.uvRect.w.ToString());
				EditorGUILayout.MinMaxSlider(curGroup.uvRect.y, curGroup.uvRect.w, 0.0, 1.0);
				GUILayout.EndHorizontal();
				curGroup.capUV = EditorGUILayout.Toggle("Use Cap UVs:", curGroup.capUV);
				if(curGroup.capUV){
					GUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("Cap UV X Range:", curGroup.capUVRect.x.ToString() + ", " + curGroup.capUVRect.z.ToString());
					EditorGUILayout.MinMaxSlider(curGroup.capUVRect.x, curGroup.capUVRect.z, 0.0, 1.0);
					GUILayout.EndHorizontal();
					GUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("Cap UV Y Range:", curGroup.capUVRect.y.ToString() + ", " + curGroup.capUVRect.w.ToString());
					EditorGUILayout.MinMaxSlider(curGroup.capUVRect.y, curGroup.capUVRect.w, 0.0, 1.0);
					GUILayout.EndHorizontal();
				}
				curGroup.createBaseline = EditorGUILayout.Toggle("Create Baseline:", curGroup.createBaseline);
				curGroup.colliderInset = EditorGUILayout.Vector4Field("Collider Inset:", curGroup.colliderInset);
				curGroup.generateBaselineColliders = EditorGUILayout.Toggle("Baseline Colliders:", curGroup.generateBaselineColliders);
				if(curGroup.createBaseline){
					GUILayout.BeginHorizontal();
					curGroup.baselineDistance = EditorGUILayout.FloatField("Baseline Distance:", curGroup.baselineDistance);
					GUILayout.EndHorizontal();
				}
			}
			
			if(GUILayout.Button("Clear Nodes")){
				curGroup.ClearNodes();
			}
			
			if(GUI.changed) {
				curGroup.UpdateMesh();
				if(curGroup.material) if(curGroup.material.mainTexture) curGroup.displayImage = curGroup.material.mainTexture;
			}
		}
	}
}

}